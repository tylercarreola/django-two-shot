from django.urls import path
from receipts.views import receipt_listview, create_receipt, account_list, category_list


urlpatterns = [
    path("category/", category_list, name="category_list"),
    path("account/", account_list, name="acount_list"),
    path('create/', create_receipt, name='create_receipt'),
    path("", receipt_listview, name="home"),
]
